//
//  MyButton.swift
//  testprojboxes
//
//  Created by Developer on 21.03.2024.
//

import SwiftUI

struct MyButton: View {
    var text: String
    var bgColor: Color
    var action: () -> Void
    
    var body: some View {
        Button {
            action()
        } label: {
            Text(text)
                .font(.system(size: 22.0))
                .foregroundStyle(.white)
                .padding(.vertical, 4.0)
                .padding(.horizontal, 8.0)
        }
        .background(bgColor)
        .cornerRadius(4.0)
    }
}
