//
//  BoxModel.swift
//  testprojboxes
//
//  Created by Developer on 21.03.2024.
//

import Foundation

struct BoxModel: Hashable {
    let width: CGFloat
    let height: CGFloat
}
