//
//  testprojboxesApp.swift
//  testprojboxes
//
//  Created by Developer on 21.03.2024.
//

import SwiftUI

@main
struct testprojboxesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: ContentViewModel())
        }
    }
}
