//
//  ContentView.swift
//  testprojboxes
//
//  Created by Developer on 21.03.2024.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel: ContentViewModel
    @State private var alignments: [Alignment] = []
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                boxesView(geometry: geometry)
                
                Spacer()
                
                buttonsView(geometry: geometry)
                .padding(.bottom, 24.0)
            }
            .background(Color.black)
        }
    }
    
    private func boxesView(geometry: GeometryProxy) -> some View {
        ScrollView(.horizontal) {
            ScrollViewReader { proxy in
                HStack {
                    ForEach(viewModel.boxes.indices, id: \.self) { index in
                        let value = viewModel.boxes[index]
                        HStack {
                            Rectangle()
                                .fill(Color.purple)
                                .frame(width: value.width, height: value.height)
                        }
                        .id(UUID())
                        .frame(height: geometry.size.height / 1.2, alignment: alignments[index])
                    }
                }
                .onChange(of: viewModel.boxes) { _, _ in
                    withAnimation {
                        proxy.scrollTo(viewModel.boxes.count - 1, anchor: .trailing)
                    }
                }
            }
        }
    }
    
    private func buttonsView(geometry: GeometryProxy) -> some View  {
        HStack {
            MyButton(text: "Start", bgColor: .green) {
                viewModel.start(maxHeight: geometry.size.height / 1.2) {
                    alignments.append(setRandomAlignment())
                }
            }
            
            MyButton(text: "Again", bgColor: .yellow) {
                viewModel.again()
                alignments.removeAll()
            }

            MyButton(text: "Pause", bgColor: .red) {
                viewModel.pause()
            }
        }
    }
}

extension ContentView {
    private func setRandomAlignment() -> Alignment {
        return [.top, .center, .bottom].randomElement()!
    }
}
