//
//  ContentViewModel.swift
//  testprojboxes
//
//  Created by Developer on 21.03.2024.
//

import Foundation

final class ContentViewModel: ObservableObject {
    @Published var boxes: [BoxModel] = []
    private var timer: Timer?
    
    func start(maxHeight: CGFloat, action: @escaping () -> Void) {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            action()
            self.boxes.append(BoxModel(width: CGFloat.random(in: 10...100),
                                       height: CGFloat.random(in: 10...maxHeight)))
        }
    }
    
    func again() {
        boxes.removeAll()
        timer?.invalidate()
        timer = nil
    }
    
    func pause() {
        timer?.invalidate()
        timer = nil
    }
}
